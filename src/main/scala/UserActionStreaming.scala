/**
  * Created by liuweixiao on 17/1/18.
  */
import java.util.Date

import models.UserActionLogEntry
import org.apache.hadoop.hbase.client.{Get, Put}
import org.apache.hadoop.hbase.util.Bytes
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.hadoop.fs.FileSystem
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.functions._
import org.json4s._
import org.json4s.JsonDSL._
import org.json4s.jackson.JsonMethods._
import org.json4s.jackson.Serialization
import org.json4s.jackson.Serialization.write
import utils._

object UserActionStreaming {

  def main(args: Array[String]): Unit = {

    /**
      * ========================================
      *   Constants and Configurable Variables
      * ========================================
      */
    // input topics
    val webPVTopic = ConfigUtil.webPVTopic
    val appPVTopic = ConfigUtil.appPVTopic
    val bbsPostTopic = ConfigUtil.bbsPostTopic
    // content extract topic
    val contentExtractTopic = ConfigUtil.contentExtractTopic
    val kafkaParams = Map("metadata.broker.list" -> ConfigUtil.kafkaBrokerList)
    val streamingIntervalSeconds =
      ConfigUtil.userActionStreamingIntervalSeconds
    // checkpoint paths
    val webPVOffsetPath = ConfigUtil.userActionWebPVOffsetPath
    val appPVOffsetPath = ConfigUtil.userActionAppPVOffsetPath
    val bbsPostOffsetPath = ConfigUtil.userActionBBSPostOffsetPath

    // Init sparkContext, streamingContext, hiveContext.
    val sparkConf = new SparkConf().setAppName("PI-UserActionStream")
    val sc = new SparkContext(sparkConf)
    sc.setLogLevel("WARN")
    val ssc = new StreamingContext(sc, Seconds(streamingIntervalSeconds))
    val hdfs = FileSystem.get(sc.hadoopConfiguration)
    val streamingUtil = new StreamingUtil(ssc, hdfs)
    val sqlContext = new SQLContext(sc)

    // object HiveContextSingleton {
    //   @transient private var instance: HiveContext = _
    //   def getInstance(sparkContext: SparkContext): HiveContext = {
    //     if (instance == null)
    //       instance = new HiveContext(sparkContext)
    //     instance
    //   }
    // }

    /**
      * ========================================
      * STREAM 1-1:   web PV kafka streams
      * ========================================
      */
    val webPVStream: DStream[(String, String)] = streamingUtil
      .createDStream(Set(webPVTopic), kafkaParams, webPVOffsetPath)

    val webPVMappedStream =
      webPVStream.map(_._2).transform { rdd =>

        // FUNC 0 ===> Mapping Log
        val logRDD =
          // If empty rdd is not ruled out, 'select' will meet exceptions.
          if (rdd.isEmpty)
            rdd.sparkContext.emptyRDD[UserActionLogEntry] // Nothing will happen here.
          else {
            var dfLog = sqlContext.read.json(rdd)

            // Add missing columns. This time only add "addons"
            val colNamesFull = Array("addons")
            for (colNotAppear <- colNamesFull.diff(dfLog.columns))
              dfLog = dfLog.withColumn(colNotAppear, lit(""))

            dfLog
              .filter(""" platform <> 5 """) // Filter out spiders !!!!!! \ _ /
              .select("username",
                      "cookie_id",
                      "atime",
                      "account",
                      "channel",
                      "action",
                      "category",
                      "ids",
                      "param",
                      "addons")
              .mapPartitions { iterator =>
                val mapper = new UserActionMapperUtil
                iterator.map(entry => mapper.webPVDocumentMap(entry))
              }
              .filter(!_.action.isEmpty) // Filter non-empty rows.
          }

        // =========
        println("\n\n")
        streamingUtil.printCount("webPVLogRDD", logRDD.cache)

        logRDD
      }

    /**
      * ========================================
      * STREAM 1-2:  app PV kafka streams
      * ========================================
      */
    val appPVStream: DStream[(String, String)] = streamingUtil
      .createDStream(Set(appPVTopic), kafkaParams, appPVOffsetPath)

    val appPVMappedStream =
      appPVStream.map(_._2).transform { rdd =>

        // FUNC 0 ===> Mapping Log
        val logRDD =
          // If empty rdd is not ruled out, 'select' will meet exceptions.
          if (rdd.isEmpty)
            rdd.sparkContext
              .emptyRDD[UserActionLogEntry] // Nothing will happen here.
          else {
            var dfLog = sqlContext.read.json(rdd)
            // Add missing columns.
            // This time only add "addons"
            val colNamesFull = Array("addons")
            for (colNotAppear <- colNamesFull.diff(dfLog.columns))
              dfLog = dfLog.withColumn(colNotAppear, lit(""))

            dfLog
              .select("dxyid",
                      "mc",
                      "atime",
                      "ac",
                      "action",
                      "api_type",
                      "category",
                      "ids",
                      "param",
                      "addons")
              .mapPartitions { iterator =>
                val mapper = new UserActionMapperUtil
                iterator.map(entry => mapper.appPVDocumentMap(entry))
              }
              .filter(!_.action.isEmpty) // Filter non-empty rows.
          }

        // =========
        streamingUtil.printCount("appPVLogRDD", logRDD.cache)

        logRDD
      }

    /**
      * ========================================
      * STREAM 1-3:  BBS Post kafka streams
      * ========================================
      */
    val bbsPostStream: DStream[(String, String)] = streamingUtil
      .createDStream(Set(bbsPostTopic), kafkaParams, bbsPostOffsetPath)

    /**
      * BBS Post Stream Format:
      * {"dxyid":"puky800","id":3693,"action":"event_post-new-success","atime":1025697390000}
      * {"dxyid":"okcxl","id":3683,"action":"event_post-reply-success","atime":1025695169000}
      */
    val bbsPostMappedStream =
      bbsPostStream.map(_._2).transform { rdd =>

        // FUNC 0 ===> Mapping Log
        val logRDD =
          // If empty rdd is not ruled out, 'select' will meet exceptions.
          if (rdd.isEmpty)
            rdd.sparkContext
              .emptyRDD[UserActionLogEntry] // Nothing will happen here.
          else {
            sqlContext.read
              .json(rdd)
              .select("dxyid", "id", "action", "atime")
              .mapPartitions { iterator =>
                val mapper = new UserActionMapperUtil
                iterator.map(entry => mapper.bbsPostDocumentMap(entry))
              }
              .filter(!_.action.isEmpty) // Filter non-empty rows.
          }

        // =========
        streamingUtil.printCount("bbsPostLogRDD", logRDD.cache)

        logRDD
      }

    /**
      * - Union webPVDStream, appPVDStream and bbsPostStream.
      * - Check HBase.
      * - Send missed documentID to kafka.
      * - Write result to HBase.
      */
    appPVMappedStream
      .union(webPVMappedStream)
      .union(bbsPostMappedStream)
      .transform { totalRDD =>
        val totalRDDWithState = totalRDD.map(_.toLogEntryWithState).cache()

        // Find missing document.
        val rddWithDocIDWithExistence = totalRDDWithState
          .filter(!_.document_id.isEmpty) // Choose logs which have docIDs.
          .mapPartitions { iterator =>
            // Set document ttl: 7 days
            val currentTimestamp = new Date().getTime
            val timestampSevenDaysBefore = currentTimestamp - (86400 * 7 * 1000)
            // Convert to entry seq because iterator is only once.
            val seqEntry = iterator.toSeq
            // Find document id existence in HBase table: pi_document_content
            val seqGet = seqEntry.map { entry =>
              val get = new Get(
                Bytes.toBytes(entry.document_id.hashCode.abs.toString
                  .take(6)
                  .reverse + "|" + entry.document_id))
              get.setTimeRange(timestampSevenDaysBefore, Long.MaxValue)
              get.setCheckExistenceOnly(true)
              get
            }
            val getResultSeq =
              HBaseUtil
                .get(seqGet.toIterator, ConfigUtil.documentContentHTable)
                .toSeq
            getResultSeq
              .map(_.getExists == true)
              .zip(seqEntry)
              .map {
                case (existence, entry) =>
                  entry.docInHbase = existence; entry
              }
              .toIterator
          }
          .cache // Cache to avoid unnecessary re-calculation.

        // =========
        streamingUtil.printCount(
          "numLogWhoseDocInHbase",
          rddWithDocIDWithExistence.filter(_.docInHbase == true))

        // =========
        streamingUtil.printCount(
          "numLogWhoseDocNotInHbase",
          rddWithDocIDWithExistence.filter(_.docInHbase == false))

        // FUNC 2 ===> Pick missing log, assemble doc_id and content, then write to kafka.
        val rddPutToKafka = rddWithDocIDWithExistence
          .filter(_.docInHbase == false)
          .map {
            case a if a.mappedAction contains "search" =>
              compact(render(
                ("contentId" -> a.document_id) ~ ("params" -> a.content)))
            case b => compact(render("contentId" -> b.document_id))
          }
          .distinct
          .cache()

        // =========
        streamingUtil.printCount("rddPutToKafka", rddPutToKafka)

        rddPutToKafka.foreachPartition { iterator =>
          KafkaOpsUtil.send(contentExtractTopic, iterator)
        }
        rddWithDocIDWithExistence.unpersist(true)
        rddPutToKafka.unpersist(true)

        // Return
        totalRDDWithState.unpersist(true)
      }
      .foreachRDD { rdd =>
        // =========
        streamingUtil.printCount("streamToHBase", rdd)
        rdd.cache()

        // Put to user action table.
        rdd.foreachPartition { iterator =>
          // Declare json format.
          implicit val formats = Serialization.formats(NoTypeHints)
          // Generate multi-puts for HBase.
          val iteratorPut = iterator.map { entry =>
            val rowkey = entry.toUserLogRowkey
            val columnQualifier = ((entry.atime + 28800000) % 86400000).toString // 28800000 is +8 hours to GMT.
            val value = write(entry.toUserActionHBaseEntry)
            val put = new Put(Bytes.toBytes(rowkey))
            put.addColumn(Bytes.toBytes("f"),
                          Bytes.toBytes(columnQualifier),
                          Bytes.toBytes(value))
          }
          // HBase operation.
          HBaseUtil.put(iteratorPut, ConfigUtil.userActionHTable)
        }

        // Put to document user action table
        rdd.filter(_.document_id.nonEmpty).foreachPartition { iterator =>
          // Declare json format.
          implicit val formats = Serialization.formats(NoTypeHints)
          // Generate multi-puts for HBase.
          val iteratorPut = iterator.map { entry =>
            val rowkey = entry.toDocumentUserActionRowkey
            val columnQualifier = ((entry.atime + 28800000) % 86400000).toString
            val value = write(entry.toUserActionHBaseEntry)
            val put = new Put(Bytes.toBytes(rowkey))
            put.addColumn(Bytes.toBytes("f"),
                          Bytes.toBytes(columnQualifier),
                          Bytes.toBytes(value))
          }
          // HBase Operation.
          HBaseUtil.put(iteratorPut, ConfigUtil.documentUserActionHTable)

        }

        rdd.unpersist(true)
      }

    // Checkpoint offset to file.
    streamingUtil.saveOffset(webPVStream, webPVOffsetPath)
    streamingUtil.saveOffset(appPVStream, appPVOffsetPath)
    streamingUtil.saveOffset(bbsPostStream, bbsPostOffsetPath)

    // Streaming start.
    ssc.start
    ssc.awaitTermination

  }

}
