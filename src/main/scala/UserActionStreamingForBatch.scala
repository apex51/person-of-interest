/**
  * Created by jianghao on 2/13/17.
  */
import java.util.Date

import models.{UserActionLogEntry, UserActionLogEntryWithState}
import org.apache.hadoop.hbase.client.{Get, Put}
import org.apache.hadoop.hbase.util.Bytes
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.functions._
import org.json4s._
import org.json4s.JsonDSL._
import org.json4s.jackson.JsonMethods._
import org.json4s.jackson.Serialization
import org.json4s.jackson.Serialization.write
import utils._

object UserActionStreamingForBatch {

  def main(args: Array[String]) {

    if (args.length < 2) {
      System.err.println("USAGE:")
      System.err.println("<0 web_pv_topic> <1 app_pv_topic>")
      System.exit(1)
    }

    // kafka params
    val webPVTopic = args(0)
    val appPVTopic = args(1)
    val hdfsFile = "pi/batch/flag/user_action"
    val contentExtractTopic = ConfigUtil.contentExtractTopic
    val kafkaParams = Map("metadata.broker.list" -> ConfigUtil.kafkaBrokerList)
    val streamingIntervalSeconds = 2
    val webPVOffsetPath = "pi/batch/checkpoint/user_action/webPV"
    val appPVOffsetPath = "pi/batch/checkpoint/user_action/appPV"

    // Init sparkContext, streamingContext, hiveContext.
    val sparkConf = new SparkConf().setAppName("PI-UserActionStreamForBatch")
    val sc = new SparkContext(sparkConf)
    sc.setLogLevel("WARN")
    val ssc = new StreamingContext(sc, Seconds(streamingIntervalSeconds))
    val hdfs = FileSystem.get(sc.hadoopConfiguration)
    val streamingUtil = new StreamingUtil(ssc, hdfs)
    val sqlContext = new SQLContext(sc)


    /**
      * ========================================
      * STREAM 1-1:   web PV kafka streams
      * ========================================
      */
    val webPVStream: DStream[(String, String)] = streamingUtil
      .createDStream(Set(webPVTopic), kafkaParams, webPVOffsetPath)

    /**
      * Format PV kafka stream:
      * - original: DStream[(String, String)]
      * - processed: DStream[(Boolean, models.OutputLogEntry)], which can be further separated into two streams.
      * |- true: can be written to HBase
      * |- false: wait for content extraction
      *
      */
    val webPVMappedStream =
    webPVStream.map(_._2).transform { rdd =>

      // FUNC 0 ===> Mapping Log
      val logRDD =
      // If empty rdd is not ruled out, 'select' will meet exceptions.
      if (rdd.isEmpty)
        rdd.sparkContext.emptyRDD[UserActionLogEntry] // Nothing will happen here.
      else {
        var dfLog = sqlContext.read.json(rdd)

        // Add missing columns. This time only add "addons"
        val colNamesFull = Array("addons")
        for (colNotAppear <- colNamesFull.diff(dfLog.columns))
          dfLog = dfLog.withColumn(colNotAppear, lit(""))

        dfLog
          .filter(""" platform <> 5 """) // Filter out spiders !!!!!! \ _ /
          .select("username",
          "cookie_id",
          "atime",
          "account",
          "channel",
          "action",
          "category",
          "ids",
          "param",
          "addons")
          .mapPartitions { iterator =>
            val mapper = new UserActionMapperUtil
            iterator.map(entry => mapper.webPVDocumentMap(entry))
          }
          .filter(!_.action.isEmpty) // Filter non-empty rows.
      }

      // =========
      println("\n\n")
      streamingUtil.printCount("webPVLogRDD", logRDD.cache)

      logRDD.unpersist(true)
    }

    /**
      * ========================================
      * STREAM 1-2:  app PV kafka streams
      * ========================================
      */
    val appPVStream: DStream[(String, String)] = streamingUtil
      .createDStream(Set(appPVTopic), kafkaParams, appPVOffsetPath)

    /**
      * Format PV kafka stream:
      * - original: DStream[(String, String)]
      * - processed: DStream[(Boolean, models.OutputLogEntry)], which can be further separated into two streams.
      * |- true: can be written to HBase
      * |- false: wait for content extraction
      *
      */
    val appPVMappedStream =
    appPVStream.map(_._2).transform { rdd =>

      // If empty rdd is not ruled out, 'select' will meet exceptions.
      if (rdd.isEmpty)
        rdd.sparkContext.emptyRDD[UserActionLogEntry] // Nothing will happen here.
      else {
        var dfLog = sqlContext.read.json(rdd)
        // Add missing columns. This time only add "addons"
        val colNamesFull = Array("addons")
        for (colNotAppear <- colNamesFull.diff(dfLog.columns))
          dfLog = dfLog.withColumn(colNotAppear, lit(""))

        dfLog
          .select("dxyid", "mc", "atime", "ac", "action", "api_type", "category", "ids", "param", "addons")
          .mapPartitions { iterator =>
            val mapper = new UserActionMapperUtil
            iterator.map(entry => mapper.appPVDocumentMap(entry))
          }
          .filter(!_.action.isEmpty) // Filter non-empty rows.
      }
    }

    /**
      * - Union webPVDStream and appPVDStream.
      * - Check HBase.
      * - Send missed documentID to kafka.
      */
    val unionStream = webPVMappedStream.union(appPVMappedStream).transform {
      totalRDD =>
        // Separate into 2 RDDs: RDDWithDocumentID & RDDWithoutDocumentID
        val totalRDDWithState = totalRDD.map(_.toLogEntryWithState).cache()

        // Find missing document.
        val rddWithDocIDWithExistence = totalRDDWithState
          .filter(!_.document_id.isEmpty) // Choose logs which have docIDs.
          .mapPartitions { iterator =>
          // Set document ttl: 7 days
          val currentTimestamp = new Date().getTime
          val timestampSevenDaysBefore = currentTimestamp - (86400 * 7 * 1000)
          // Convert to entry seq because iterator is only once.
          val seqEntry = iterator.toSeq
          // Find document id existence in HBase table: pi_document_content
          val seqGet = seqEntry.map { entry =>
            val get = new Get(
              Bytes.toBytes(entry.document_id.hashCode.abs.toString
                .take(6)
                .reverse + "|" + entry.document_id))
            get.setTimeRange(timestampSevenDaysBefore, Long.MaxValue)
            get.setCheckExistenceOnly(true)
            get
          }
          val getResultSeq = HBaseUtil.get(seqGet.toIterator, ConfigUtil.documentContentHTable).toSeq

          getResultSeq
            .map(_.getExists == true)
            .zip(seqEntry)
            .map {
              case (existence, entry) =>
                entry.docInHbase = existence; entry
            }
            .toIterator
        }
          .cache // Cache to avoid unnecessary re-calculation.

        // =========
        streamingUtil.printCount("numLogWhoseDocInHbase", rddWithDocIDWithExistence.filter(_.docInHbase == true))

        // =========
        streamingUtil.printCount("numLogWhoseDocNotInHbase", rddWithDocIDWithExistence.filter(_.docInHbase == false))

        // FUNC 2 ===> Pick missing log, assemble doc_id and content, then write to kafka.
        val rddPutToKafka = rddWithDocIDWithExistence
          .filter(_.docInHbase == false)
          .map {
            case a if a.mappedAction contains "search" =>
              compact(render(
                ("contentId" -> a.document_id) ~ ("params" -> a.content)))
            case b => compact(render("contentId" -> b.document_id))
          }
          .distinct
          .cache()

        // =========
        streamingUtil.printCount("rddPutToKafka", rddPutToKafka)

        rddPutToKafka.foreachPartition { iterator =>
          KafkaOpsUtil.send(contentExtractTopic, iterator)
        }
        rddWithDocIDWithExistence.unpersist(true)
        rddPutToKafka.unpersist(true)

        // Return
        totalRDDWithState.unpersist(true)
    }

    /**
      * ================
      * Convenient func for terminating the job from outside.
      * - job can not be terminated inside the job.
      */
    // This variable is used to remember how many times rdd to HBase is zero.
    // If this variable is more than 3, stop the streaming.
    var batchCount = 0
    // Flag to indicate if batch is started.
    // Before batch started, there may be several empty batches when log jobs are busy generating data into kafka.
    var batchStartFlag = false
    // Batch started because data begin to pour into HDFS.
    def updateState(rdd: RDD[UserActionLogEntryWithState]): Unit = {
      if (!rdd.isEmpty()) batchStartFlag = true
      if (batchStartFlag && rdd.isEmpty()) batchCount += 1
      if (batchStartFlag && batchCount > 3) {
        val os = hdfs.create(new Path(hdfsFile))
        os.writeChars("done")
        os.close()
      }
    }

    // stream to hbase operation
    unionStream.foreachRDD { rdd =>
      // =========
      streamingUtil.printCount("streamToHBase", rdd.cache())
      // ========= update flag
      updateState(rdd)

      // Put to user action table.
      rdd.foreachPartition { iterator =>
        // Declare json format.
        implicit val formats = Serialization.formats(NoTypeHints)
        // Generate multi-puts for HBase.
        val iteratorPut = iterator.map { entry =>
          val rowkey = entry.toUserLogRowkey
          val columnQualifier = ((entry.atime + 28800000) % 86400000).toString // 28800000 is +8 hours to GMT.
        val value = write(entry.toUserActionHBaseEntry)
          val put = new Put(Bytes.toBytes(rowkey))
          put.addColumn(Bytes.toBytes("f"),
            Bytes.toBytes(columnQualifier),
            Bytes.toBytes(value))
        }
        // HBase operation.
        HBaseUtil.put(iteratorPut, ConfigUtil.userActionHTable)
      }

      // Put to document user action table
      rdd.filter(_.document_id.nonEmpty).foreachPartition { iterator =>
        // Declare json format.
        implicit val formats = Serialization.formats(NoTypeHints)
        // Generate multi-puts for HBase.
        val iteratorPut = iterator.map { entry =>
          val rowkey = entry.toDocumentUserActionRowkey
          val columnQualifier = ((entry.atime + 28800000) % 86400000).toString
          val value = write(entry.toUserActionHBaseEntry)
          val put = new Put(Bytes.toBytes(rowkey))
          put.addColumn(Bytes.toBytes("f"),
            Bytes.toBytes(columnQualifier),
            Bytes.toBytes(value))
        }
        // HBase Operation.
        HBaseUtil.put(iteratorPut, ConfigUtil.documentUserActionHTable)

      }

      rdd.unpersist(true)
    }

    // Checkpoint offset to file.
    streamingUtil.saveOffset(webPVStream, webPVOffsetPath)
    streamingUtil.saveOffset(appPVStream, appPVOffsetPath)

    // Streaming start.
    ssc.start
    ssc.awaitTermination
  }

}
