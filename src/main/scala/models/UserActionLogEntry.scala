package models

/**
  * Created by jianghao on 12/6/16.
  */
// User Action format for internal-code use.
case class UserActionLogEntry(var tag: String,
                              var tagType: String,
                              var atime: Long,
                              var action: String,
                              var mappedAction: String,
                              var document_id: String,
                              var content: String,
                              var account: String) {
  // Transformation
  def toLogEntryWithState = UserActionLogEntryWithState(tag, tagType, atime, action, mappedAction, document_id, content, account,
       docInHbase = false)
}
