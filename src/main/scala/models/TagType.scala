package models

/**
  * Created by jianghao on 3/3/17.
  */
object TagType extends Enumeration {
  val MC_TYPE = "MC"
  val C_TYPE = "C"
  val U_TYPE = "U"
}
