package models

import java.util.Date

import utils.HBaseUtil


/**
  * Created by jianghao on 3/2/17.
  */
// User Action format with state
case class UserActionLogEntryWithState(
    var tag: String,
    var tagType: String,
    var atime: Long,
    var action: String,
    var mappedAction: String,
    var document_id: String,
    var content: String,
    var account: String,
    var docInHbase: Boolean // Check if doc in HBase, if not put to kafka.
) {
  // Get rowkey for user log table.
  def toUserLogRowkey = HBaseUtil.piUserLogRowkey(tagType, tag, new Date(atime))

  // Get rowkey for document user action table.
  def toDocumentUserActionRowkey = HBaseUtil.piDocumentUserActionRowkey(document_id, new Date(atime), tagType, tag)

  // Transformation.
  def toUserActionHBaseEntry = UserActionToHBaseEntry(atime, action, document_id, content, account)
}
