package models

import org.json4s.NoTypeHints
import org.json4s.jackson.Serialization

/**
  * Created by jianghao on 3/2/17.
  */
// User Action for hbase output using json.
case class UserActionToHBaseEntry(var atime: Long,
                                  var action: String,
                                  var document_id: String,
                                  var content: String,
                                  var account: String) {


}
