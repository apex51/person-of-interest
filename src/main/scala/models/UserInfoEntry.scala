package models

/**
  * Created by jianghao on 3/2/17.
  */
// User Info output format using json.
case class UserInfoEntry(var dxyid: String,
                         var cookie_id: Option[String],
                         var mc: Option[String])
