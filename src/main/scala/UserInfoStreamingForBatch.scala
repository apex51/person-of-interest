/**
  * Created by jianghao on 2/13/17.
  */

import models.{UserActionLogEntryWithState, UserInfoEntry, UserInfoToHBaseEntry}
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.sql.{Row, SQLContext}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.hive.HiveContext
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.Seconds
import org.apache.spark.streaming.dstream.DStream
import org.apache.hadoop.hbase.client.Put
import org.apache.hadoop.hbase.util.Bytes
import utils.{ConfigUtil, HBaseUtil, StreamingUtil}
import models.TagType._
import org.apache.spark.rdd.RDD


object UserInfoStreamingForBatch {

  def main(args: Array[String]) {

    if (args.length < 3) {
      System.err.println("USAGE:")
      System.err.println(
        "<0 web_pv_topic> <1 app_pv_topic> <2 app_ev_topic>")
      System.exit(1)
    }

    // kafka params
    val webPVTopic = args(0)
    val appPVTopic = args(1)
    val appEVTopic = args(2)
    val hdfsFile = "pi/batch/flag/user_info"
    val kafkaParams = Map("metadata.broker.list" -> ConfigUtil.kafkaBrokerList)
    val streamingIntervalSeconds = 2
    val appStreamOffsetPath = "pi/batch/checkpoint/user_info/app_stream"
    val webStreamOffsetPath = "pi/batch/checkpoint/user_info/web_stream"

    /**
      * Init spark contexts and streaming contexts.
      */
    val sparkConf = new SparkConf()
      .setAppName("PI-UserIdentityLinkingForBatch")
    val sc = new SparkContext(sparkConf)
    sc.setLogLevel("WARN")
    val ssc = new StreamingContext(sc, Seconds(streamingIntervalSeconds))
    val hdfs = FileSystem.get(sc.hadoopConfiguration)
    val streamingUtil = new StreamingUtil(ssc, hdfs)
    val sqlContext = new SQLContext(sc)

    /**
      * Create kafka streams.
      */
    // App Stream, including app event & app pv
    val appStream: DStream[(String, String)] = streamingUtil.createDStream(
      Set(appPVTopic, appEVTopic),
      kafkaParams,
      appStreamOffsetPath)
    // Web Stream, i.e. web pv
    val webStream: DStream[(String, String)] = streamingUtil
      .createDStream(Set(webPVTopic), kafkaParams, webStreamOffsetPath)

    /**
      * Process streams.
      */
    // app stream
    val appRowkeyStream = appStream.map(_._2).transform { rdd =>
      if (rdd.cache.isEmpty) sc.emptyRDD[(UserInfoToHBaseEntry, UserInfoEntry)]
      else
        sqlContext.read
          .json(rdd)
          .select("dxyid", "mc")
          .filter(""" mc <> '' and dxyid <> ''""")
          .map {
            case Row(dxyid: String, mc: String) =>
              val tag = mc.toLowerCase
              (UserInfoToHBaseEntry(HBaseUtil.piUserInfoRowkey(dxyid, MC_TYPE, tag), dxyid),
                UserInfoEntry(dxyid, None, Some(tag)))
          }
    }

    // web stream
    val webRowkeyStream = webStream.map(_._2).transform { rdd =>
      if (rdd.cache.isEmpty) sc.emptyRDD[(UserInfoToHBaseEntry, UserInfoEntry)]
      else
        sqlContext.read
          .json(rdd)
          .select("username", "cookie_id")
          .filter(""" username <> '' and cookie_id <> ''""")
          .map {
            case Row(dxyid: String, cookie_id: String) =>
              val tag = cookie_id.toLowerCase
              (UserInfoToHBaseEntry(HBaseUtil.piUserInfoRowkey(dxyid, C_TYPE, tag), dxyid),
                UserInfoEntry(dxyid, Some(tag), None))
          }
    }


    /**
      * Write each rdd to HBase
      */

    // This variable is used to remember how many times no data is written to HBase.
    // If this variable is more than 3, stop the streaming.
    var batchCount = 0
    // Flag to indicate if batch is started.
    // Before batch started, there may be several empty batches when log jobs are busy generating data into kafka.
    var batchStartFlag = false
    // Batch started because data begin to pour into HBase.
    def updateState(rdd: RDD[(UserInfoToHBaseEntry, UserInfoEntry)]): Unit = {
      if (!rdd.isEmpty()) batchStartFlag = true
      if (batchStartFlag && rdd.isEmpty()) batchCount += 1
      if (batchStartFlag && batchCount > 3) {
        val os = hdfs.create(new Path(hdfsFile))
        os.writeChars("done")
        os.close()
      }
    }


    val mergedRowkeyStream = appRowkeyStream.union(webRowkeyStream)
    // Write to HBase
    mergedRowkeyStream.foreachRDD { rdd =>

      updateState(rdd)

      // Write to HBase
      val rddToHBase = rdd.map(_._1).distinct
      println(s" === HBase Queue Length: ${rddToHBase.count()}")

      rddToHBase.foreachPartition { iterator =>
        // Convert to HBase input.
        val iteratorPut = iterator.map { entry =>
          val put = new Put(Bytes.toBytes(entry.rowkey))
          put.addColumn(
            Bytes.toBytes("f"),
            Bytes.toBytes("dxyid"),
            Bytes.toBytes(entry.dxyid)) // Add a useless row: rowkey with useless row value.
        }
        // Multi put to HBase.
        HBaseUtil.put(iteratorPut, ConfigUtil.userInfoHTable)
      }

    }


    /**
      * Checkpoint offset to file
      */
    streamingUtil.saveOffset(appStream, appStreamOffsetPath)
    streamingUtil.saveOffset(webStream, webStreamOffsetPath)

    ssc.start
    ssc.awaitTermination

  }
}
