/**
  * Created by jianghao on 12/12/16.
  */

import models.{UserInfoEntry, UserInfoToHBaseEntry}
import org.apache.hadoop.fs.FileSystem
import org.apache.spark.sql.{Row, SQLContext}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.Seconds
import org.apache.spark.streaming.dstream.DStream
import org.apache.hadoop.hbase.client.Put
import org.apache.hadoop.hbase.util.Bytes
import utils._
import models.TagType._


object UserInfoStreaming {

  def main(args: Array[String]) {

    /**
      * === Constants and Configurable Variables ===
      */
    // input topics
    val webPVTopic = ConfigUtil.webPVTopic
    val appPVTopic = ConfigUtil.appPVTopic
    val appEVTopic = ConfigUtil.appEVTopic
    val bbsPostTopic = ConfigUtil.bbsPostTopic
    // output topics
    val userInfoTopic = ConfigUtil.userInfoTopic
    val kafkaParams = Map("metadata.broker.list" -> ConfigUtil.kafkaBrokerList)
    val streamingIntervalSeconds = ConfigUtil.userInfoStreamingIntervalSeconds
    // offset checkpointing
    val appStreamOffsetPath = ConfigUtil.userInfoAppStreamOffsetPath
    val webStreamOffsetPath = ConfigUtil.userInfoWebStreamOffsetPath
    val bbsPostStreamOffsetPath = ConfigUtil.userInfoBBSPostStreamOffsetPath

    /**
      * Init spark contexts and streaming contexts.
      */
    val sparkConf = new SparkConf().setAppName("PI-UserIdentityLinking")
    val sc = new SparkContext(sparkConf)
    sc.setLogLevel("WARN")
    val ssc = new StreamingContext(sc, Seconds(streamingIntervalSeconds))
    val hdfs = FileSystem.get(sc.hadoopConfiguration)
    val streamingUtil = new StreamingUtil(ssc, hdfs)
    val sqlContext = new SQLContext(sc)

    /**
      * Lazily instantiated singleton instance of HiveContext
      */
    // object SqlContextSingleton {
    //   @transient private var instance: SQLContext = _
    //
    //   def getInstance(sparkContext: SparkContext): SQLContext = {
    //     if (instance == null) {
    //       instance = new SQLContext(sparkContext)
    //     }
    //     instance
    //   }
    // }

    /**
      * Create kafka streams.
      */
    // App Stream, including app event & app pv
    val appStream: DStream[(String, String)] = streamingUtil.createDStream(
      Set(appPVTopic, appEVTopic),
      kafkaParams,
      appStreamOffsetPath)
    // Web Stream, i.e. web pv
    val webStream: DStream[(String, String)] = streamingUtil
      .createDStream(Set(webPVTopic), kafkaParams, webStreamOffsetPath)
    // BBS Post Stream
    val bbsPostStream: DStream[(String, String)] = streamingUtil
      .createDStream(Set(bbsPostTopic), kafkaParams, bbsPostStreamOffsetPath)


    /**
      * Process streams.
      */
    // app stream
    val appRowkeyStream = appStream.map(_._2).transform { rdd =>
      // println(hiveContext)
      val returnRDD =
        if (rdd.isEmpty) sc.emptyRDD[(UserInfoToHBaseEntry, UserInfoEntry)]
        else
          sqlContext.read
            .json(rdd)
            .select("dxyid", "mc")
            .filter(""" mc <> '' and dxyid <> ''""")
            .map {
              case Row(dxyid: String, mc: String) =>
                val tag = mc.toLowerCase
                (UserInfoToHBaseEntry(HBaseUtil.piUserInfoRowkey(dxyid, MC_TYPE, tag), dxyid),
                 UserInfoEntry(dxyid, None, Some(tag)))
            }
      returnRDD
    }

    // web stream
    val webRowkeyStream = webStream.map(_._2).transform { rdd =>
      // println(hiveContext)
      val returnRDD =
        if (rdd.isEmpty) sc.emptyRDD[(UserInfoToHBaseEntry, UserInfoEntry)]
        else
          sqlContext.read
            .json(rdd)
            .select("username", "cookie_id")
            .filter(""" username <> '' and cookie_id <> ''""")
            .map {
              case Row(dxyid: String, cookie_id: String) =>
                val tag = cookie_id.toLowerCase
                (UserInfoToHBaseEntry(HBaseUtil.piUserInfoRowkey(dxyid, C_TYPE, tag), dxyid),
                 UserInfoEntry(dxyid, Some(tag), None))
            }
      returnRDD
    }

    // bbs post stream
    val bbsPostRowkeyStream = bbsPostStream.map(_._2).transform { rdd =>
      val returnRDD =
        if (rdd.isEmpty) sc.emptyRDD[(UserInfoToHBaseEntry, UserInfoEntry)]
        else
          sqlContext.read
            .json(rdd)
            .select("dxyid")
            .filter(""" dxyid <> '' """)
            .map {
              case Row(dxyid: String) =>
                val tag = HBaseUtil.stringToMD5(dxyid)
                (UserInfoToHBaseEntry(HBaseUtil.piUserInfoRowkey(dxyid, U_TYPE, tag), dxyid),
                 UserInfoEntry(dxyid, None, None))
            }
      returnRDD
    }

    /**
      * Write each rdd to HBase
      */
    val mergedRowkeyStream =
      appRowkeyStream.union(webRowkeyStream).union(bbsPostRowkeyStream)
    // Write to HBase
    mergedRowkeyStream.foreachRDD { rdd =>
      // Cache data for duplicate use.
      rdd.cache

      // Write to HBase
      val rddToHBase = rdd.map(_._1).distinct
      println(s" === HBase Queue Length: ${rddToHBase.count()}")

      rddToHBase.foreachPartition { iterator =>
        // Convert to HBase input.
        val iteratorPut = iterator.map { entry =>
          val put = new Put(Bytes.toBytes(entry.rowkey))
          put.addColumn(
            Bytes.toBytes("f"),
            Bytes.toBytes("dxyid"),
            Bytes.toBytes(entry.dxyid)) // Add a useless row: rowkey with useless row value.
        }
        // Multi put to HBase.
        HBaseUtil.put(iteratorPut, ConfigUtil.userInfoHTable)
      }

      // Write to kafka
      val rddToKafka = rdd.map(_._2)
      rddToKafka.foreachPartition { iterator =>
        iterator.map(UserInfoUtil.userInfoToJSON)
        KafkaOpsUtil.send(userInfoTopic,
                          iterator.map(UserInfoUtil.userInfoToJSON))
      }
      rdd.unpersist(true)
    }

    /**
      * Checkpoint offset to file
      */
    streamingUtil.saveOffset(appStream, appStreamOffsetPath)
    streamingUtil.saveOffset(webStream, webStreamOffsetPath)
    streamingUtil.saveOffset(bbsPostStream, bbsPostStreamOffsetPath)

    ssc.start
    ssc.awaitTermination

  }
}
