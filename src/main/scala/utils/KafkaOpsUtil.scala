package utils

/**
  * Created by jianghao on 12/29/16.
  */

import java.util.Properties

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}

object KafkaOpsUtil {

  val props = new Properties()
  props.put("bootstrap.servers", ConfigUtil.kafkaBrokerList)
  props.put("key.serializer",
            "org.apache.kafka.common.serialization.StringSerializer")
  props.put("value.serializer",
            "org.apache.kafka.common.serialization.StringSerializer")

  lazy val producer = new KafkaProducer[String, String](props)
  // val TOPIC = ConfigUtil.contentExtractTopic

  def send(topicName: String, iteratorSend: Iterator[String]): Unit = {
    iteratorSend.foreach { record =>
      producer.send(new ProducerRecord[String, String](topicName, record))
    }
  }

  def close(): Unit = {
    producer.close()
  }

}
