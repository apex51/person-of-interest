package utils

/**
  * Created by jianghao on 12/29/16.
  */
import java.security.MessageDigest
import java.text.SimpleDateFormat
import java.util.Date

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.TableName
import org.apache.hadoop.hbase.client.{ConnectionFactory, Get, Put, Result}
import org.apache.hadoop.hbase.util.Bytes

import scala.collection.JavaConverters._

object HBaseUtil {

  //

  // Init connection.
  val conf = new Configuration()
  conf.set("hbase.zookeeper.quorum", ConfigUtil.zkQuorum)
  conf.set("hbase.zookeeper.property.clientPort", ConfigUtil.zkClientPort)
  lazy val conn = ConnectionFactory.createConnection(conf)

  val simpleDateFormat = new SimpleDateFormat("yyyyMMdd")

  // Init md5.
  val md5 = MessageDigest.getInstance("MD5")
  // Function: stringToMD5.
  val stringToMD5 = (dxyid: String) =>
    md5.digest(dxyid.getBytes("UTF-8")).map("%02x".format(_)).mkString

  // Generate rowkey for pi_user_info.
  def piUserInfoRowkey(dxyid: String, tagType: String, tag: String) = {
    val identity = tagType + ":" + tag
    stringToMD5(dxyid) + ":" + identity
  }

  // Generate rowkey for pi_user_log.
  def piUserLogRowkey(tagType: String, tag: String, date: Date) = {
    val identity = tagType + ":" + tag
    identity.hashCode.abs.toString.take(6).reverse + ":" + identity + ":" + simpleDateFormat.format(date)
  }

  // Generate rowkey for pi_document_user_action.
  def piDocumentUserActionRowkey(documentId: String, date: Date, tagType: String, tag: String) = {
    val identity = tagType + ":" + tag
    documentId.hashCode.abs.toString.take(6).reverse + ":" + documentId + ":" + simpleDateFormat.format(date) + ":" + identity
  }

  /**
    * Multi Put
    * @param iteratorPut
    * @param tableName
    */
  def put(iteratorPut: Iterator[Put], tableName: String): Unit = {
    val hTableName = Bytes.toBytes(tableName)
    val table = conn.getTable(TableName.valueOf(hTableName))
    table.put(iteratorPut.toBuffer.asJava)
    table.close()
  }

  /**
    * Multi Get
    * @param iterator
    * @param tableName
    * @return
    */
  def get(iterator: Iterator[Get], tableName: String): Iterator[Result] = {
    val hTableName = Bytes.toBytes(tableName)
    val table = conn.getTable(TableName.valueOf(hTableName))
    val iteratorGet = table.get(iterator.toBuffer.asJava).toIterator
    table.close()
    iteratorGet
  }

  /**
    * Close connection.
    */
  def close(): Unit = {
    conn.close()
  }

}
