package utils

/**
  * Created by jianghao on 2/14/17.
  */

import models.UserInfoEntry
import org.json4s.NoTypeHints
import org.json4s.jackson.Serialization
import org.json4s.jackson.Serialization.write

object UserInfoUtil {

  implicit val formats = Serialization.formats(NoTypeHints)

  // Transfer models.UserInfoEntry to JSON String.
  def userInfoToJSON(userInfo: UserInfoEntry): String = {
    write(userInfo)
  }

}
