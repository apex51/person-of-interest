package utils


/**
  * Created by jianghao on 12/27/16.
  */

import java.io.{ObjectInputStream, ObjectOutputStream}

import kafka.common.TopicAndPartition
import kafka.message.MessageAndMetadata
import kafka.serializer.StringDecoder
import models.UserActionLogEntryWithState
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.dstream.{DStream, InputDStream}
import org.apache.spark.streaming.kafka.{HasOffsetRanges, KafkaUtils, OffsetRange}

class StreamingUtil(val ssc: StreamingContext, val hdfs: FileSystem) {

  /**
    * Read missed logs from cached file, or create an empty RDD out of nowhere.
    * @param cachedRDDPath
    * @return
    */
  def createOrReadCachedRDD(cachedRDDPath: String): RDD[UserActionLogEntryWithState] = {
    if (hdfs.exists(new Path(cachedRDDPath)))
      ssc.sparkContext.objectFile[UserActionLogEntryWithState](s"$cachedRDDPath/*", 4) // Further Change ================================
    else ssc.sparkContext.parallelize(Seq[UserActionLogEntryWithState]())
  }

  /**
    * Save missed logs to cached file.
    * @param rdd
    * @param cachedRDDPath
    * @tparam T
    */
  def saveCachedRDD[T](rdd: RDD[T], cachedRDDPath: String): Unit = {
    val path = new Path(cachedRDDPath)
    if (hdfs.exists(path))
      hdfs.delete(path, true)
    rdd.saveAsObjectFile(cachedRDDPath)
  }

  /**
    * Create DStream from checkpoint file or out of nowhere!
    * @param topicSet
    * @param kafkaParams
    * @param offsetPath
    * @return
    */
  def createDStream(topicSet: Set[String],
                    kafkaParams: Map[String, String],
                    offsetPath: String): InputDStream[(String, String)] = {

    if (hdfs.exists(new Path(offsetPath))) {
      // Read offsetRanges from file.
      val ois = new ObjectInputStream(hdfs.open(new Path(offsetPath)))
      val offsetRanges = ois.readObject.asInstanceOf[Array[OffsetRange]]
      ois.close()
      // set offsetMap
      var fromOffsets = Map[TopicAndPartition, Long]()
      for (o <- offsetRanges)
        fromOffsets = fromOffsets + (o.topicAndPartition -> o.untilOffset)
      val extractKeyValue = (msgAndMeta: MessageAndMetadata[String, String]) =>
        (msgAndMeta.key, msgAndMeta.message)
      KafkaUtils.createDirectStream[String,
                                    String,
                                    StringDecoder,
                                    StringDecoder,
                                    (String, String)](ssc,
                                                      kafkaParams,
                                                      fromOffsets,
                                                      extractKeyValue)
    } else {
      KafkaUtils
        .createDirectStream[String, String, StringDecoder, StringDecoder](
          ssc,
          kafkaParams,
          topicSet)
    }
  }

  /**
    * Checkpoint kafka stream offset to file.
    * @param destPath
    * @param offsetRanges
    */
  def saveOffset(destPath: String, offsetRanges: Array[OffsetRange]): Unit = {
    val offsetOS = new ObjectOutputStream(hdfs.create(new Path(destPath)))
    offsetOS.writeObject(offsetRanges)
    offsetOS.close()
  }

  /**
    * Checkpoint kafka stream offset to file.
    * @param stream
    * @param destPath
    * @tparam T
    */
  def saveOffset[T](stream: DStream[T], destPath: String): Unit = {
    stream.foreachRDD { rdd =>
      println(s" ====== Checkpointing to $destPath ====== ")
      val offsetOS = new ObjectOutputStream(hdfs.create(new Path(destPath)))
      offsetOS.writeObject(rdd.asInstanceOf[HasOffsetRanges].offsetRanges)
      offsetOS.close()
    }
  }

  def printCount[T](rddName: String, rdd: RDD[T]): Unit = {
    println(s" ====== $rddName count: ${rdd.count} ====== ")
  }

}
