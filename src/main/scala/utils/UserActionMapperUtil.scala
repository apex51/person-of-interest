package utils

/**
  * Created by jianghao on 12/6/16.
  */
import java.security.MessageDigest
import java.util.{List => JList, Map => JMap}

import models.UserActionLogEntry
import org.apache.spark.sql.Row

import scala.util.Try
import models.TagType._

class UserActionMapperUtil {

  val md = MessageDigest.getInstance("MD5")

  // Read yaml config from file.
  // val yaml = new Yaml(new SafeConstructor)
  // convenient func to handle external files, using loan pattern
  // def using[A](r: Source)(f: Source => A): A =
  //   try f(r)
  //   finally r.close()
  // Get sources and parse to scala data structure.
  // read yaml for web
  // val webYAMLConfig: Map[String, List[String]] = yaml
  //   .load(
  //     using(Source.fromURL(getClass.getResource("web_actions.yaml")))(
  //       _.mkString))
  //   .asInstanceOf[JMap[String, JList[String]]]
  //   .asScala
  //   .toMap
  //   .mapValues(_.asScala.toList)
  // read yaml for app
  // val appYAMLConfig: Map[String, List[String]] = yaml
  //   .load(
  //     using(Source.fromURL(getClass.getResource("app_actions.yaml")))(
  //       _.mkString))
  //   .asInstanceOf[JMap[String, JList[String]]]
  //   .asScala
  //   .toMap
  //   .mapValues(_.asScala.toList)

  /**
    * Convenient func: def value cast to String function.
    */
  def fieldValueToString(fieldName: String, currentRow: Row): String = {
    val fieldValue = currentRow(currentRow.fieldIndex(fieldName))
    fieldValue match {
      case i if i == null => ""
      case i: String => i
      case _ => fieldValue.toString
    }
  }

  /**
    * Check business attrs and map to: username, tag, atime, document_id, search_words
    */
  def webPVDocumentMap(row: Row): UserActionLogEntry = {

    var Row(username: String,
            cookie_id: String,
            atime: Long,
            account: String,
            channel: String,
            action: String,
            category: String,
            ids: Seq[Long],
            param: String,
            _) = row

    val addons =
      if (fieldValueToString("addons", row).isEmpty) Row()
      else row.getStruct(row.fieldIndex("addons"))

    // Init return value.
    val logEntry = UserActionLogEntry("", "", 0L, "", "", "", "", "")

    // Check if cookie_id is empty.
    if (cookie_id.isEmpty) return logEntry

    // Wash _<page> away from action:
    // e.g. list_dmall-gift_1 -> list_dmall-gift
    action = action.replaceAll("""_\d+$""", "")

    // Check if account and action combination is in yaml. If no, return empty.
    // if (!webYAMLConfig.getOrElse(account, List[String]()).contains(action))
    //   return logEntry
    // Skip this because need not to exclude other account.

    // Set values:
    // account, delete da- prefix
    // tag, add C_=
    // atime
    // action, as a reminder, action's _page is cleaned
    logEntry.account = account.replace("da-", "")
    logEntry.tagType = C_TYPE
    logEntry.tag = cookie_id.toLowerCase // Need to change to lower case, then add tag-tag C_=.
    logEntry.atime = atime
    logEntry.action = action

    // Map specific account-action combination, for content extraction.
    (account, action) match {
      // ============ bbs da-10002-4
      case ("da-10002-4", "show_post-topic") =>
        logEntry.mappedAction = "read"
        logEntry.document_id = "bbs_thread" + "|" + ids.head
      case ("da-10002-4", "show_post-thread") =>
        logEntry.mappedAction = "read"
        logEntry.document_id = "bbs_thread" + "|" + ids.head
      // ============ jobmd da-10001-1
      case ("da-10001-1", "show_company") =>
        logEntry.mappedAction = "read"
        logEntry.document_id = "jobmd_company" + "|" + ids.head
      case ("da-10001-1", "show_work") =>
        logEntry.mappedAction = "read"
        logEntry.document_id = "jobmd_work" + "|" + ids.head
      case ("da-10001-1", "list_article-search") =>
        val keyword =
          Try(addons.getAs[String]("keywords").toString).getOrElse("")
        logEntry.mappedAction = "search"
        logEntry.document_id = "jobmd_search" + "|" + md
            .digest(keyword.getBytes("UTF-8"))
            .map("%02x".format(_))
            .mkString
        logEntry.content = keyword
      case ("da-10001-1", "show_article") =>
        logEntry.mappedAction = "read"
        logEntry.document_id = "jobmd_article" + "|" + ids.head
      // ============ biomart da-10000-1
      case ("da-10000-1", "show_news") =>
        logEntry.mappedAction = "read"
        logEntry.document_id = "biomart_news" + "|" + ids.head
      case ("da-10000-1", "show_infosupply") =>
        logEntry.mappedAction = "read"
        logEntry.document_id = "biomart_infosupply" + "|" + ids.head
      case ("da-10000-1", act) if act.startsWith("list_search") =>
        val keyword = Try(addons.getAs[String]("wd").toString).getOrElse("")
        logEntry.mappedAction = "search"
        logEntry.document_id = "biomart_search" + "|" + md
            .digest(keyword.getBytes("UTF-8"))
            .map("%02x".format(_))
            .mkString
        logEntry.content = keyword
      // ============ drug
      case ("da-10002-9", "show_drug") =>
        logEntry.mappedAction = "read"
        logEntry.document_id = "drug" + "|" + ids.head
      case ("da-10002-9", act) if act.startsWith("list_search") =>
        val keyword =
          Try(addons.getAs[String]("keyword").toString).getOrElse("")
        logEntry.mappedAction = "search"
        logEntry.document_id = "drug_search" + "|" + md
            .digest(keyword.getBytes("UTF-8"))
            .map("%02x".format(_))
            .mkString
        logEntry.content = keyword
      case ("da-10002-9", "show_info") =>
        logEntry.mappedAction = "read"
        logEntry.document_id = "drug_article" + "|" + ids.head
      // === drug da-10003-2
      case ("da-10003-2", "show_drug") =>
        logEntry.mappedAction = "read"
        logEntry.document_id = "drug" + "|" + ids.head
      case ("da-10003-2", "list_search") =>
        val keyword =
          Try(addons.getAs[String]("keyword").toString).getOrElse("")
        logEntry.mappedAction = "search"
        logEntry.document_id = "drug_search" + "|" + md
            .digest(keyword.getBytes("UTF-8"))
            .map("%02x".format(_))
            .mkString
        logEntry.content = keyword
      // === cms da-10002-1
      case ("da-10002-1", "show_article") =>
        logEntry.mappedAction = "read"
        logEntry.document_id = "cms_article" + "|" + ids.head
      case ("da-10002-1", "list_search") =>
        val keyword =
          Try(addons.getAs[String]("keywords").toString).getOrElse("")
        logEntry.mappedAction = "search"
        logEntry.document_id = "cms_search" + "|" + md
            .digest(keyword.getBytes("UTF-8"))
            .map("%02x".format(_))
            .mkString
        logEntry.content = keyword
      // === cms da-10002-16
      case ("da-10002-16", "show_special-article") =>
        logEntry.mappedAction = "read"
        logEntry.document_id = "cms_article" + "|" + ids.head
      case ("da-10002-16", act) if act.startsWith("list_search") =>
        val keyword =
          Try(addons.getAs[String]("keywords").toString).getOrElse("")
        logEntry.mappedAction = "search"
        logEntry.document_id = "cms_search" + "|" + md
            .digest(keyword.getBytes("UTF-8"))
            .map("%02x".format(_))
            .mkString
        logEntry.content = keyword
      // === search da-10002-7
      case ("da-10002-7", "list_search") =>
        val keyword = Try(addons.getAs[String]("words").toString).getOrElse("")
        logEntry.mappedAction = "search"
        logEntry.document_id = "search" + "|" + md
            .digest(keyword.getBytes("UTF-8"))
            .map("%02x".format(_))
            .mkString
        logEntry.content = keyword
      case other_cases =>
    }

    logEntry // return a log entry
  }

  /**
    * app rules corresponding to webPvDocumentMap
    */
  def appPVDocumentMap(row: Row): UserActionLogEntry = {

    var Row(dxyid: String,
            mc: String, // for PV, use mc
            atime: Long,
            ac: Long, // business code
            action: String,
            apiType: Seq[String],
            category: String,
            ids: Seq[Long],
            param: String,
            _) = row

    // val addons =
    //   if (fieldValueToString("addons", row).isEmpty) Row()
    //   else row.getStruct(row.fieldIndex("addons"))

    // Init return value.
    val logEntry = UserActionLogEntry("", "", 0L, "", "", "", "", "")

    // Check if mc is empty.
    if (mc.isEmpty) return logEntry

    // Re-assemble action from action and apiType
    // e.g. list_search + ["search", "indication"] -> list_search-indication
    action = action.split("_").head + "_" + apiType.mkString("-")

    // Check if account and action combination is in yaml. If no, return empty.
    // if (!appYAMLConfig.getOrElse(ac.toString, List[String]()).contains(action))
    //   return logEntry
    // Skip this because need not to exclude other account.

    // Set values.
    logEntry.account = ac match {
      case 8 | 9 => "bbs"
      case 1 | 2 | 3 | 10 => "dotcom"
      case 4 | 5 => "drugs"
      case 6 | 7 => "medtime"
      case _ => return logEntry; "unknown"
    }
    logEntry.atime = atime
    logEntry.action = action
    logEntry.tagType = MC_TYPE
    logEntry.tag = mc.toLowerCase

    (ac, action) match {
      // ============ ac = 8 or 9
      case (8 | 9, "show_post") =>
        logEntry.mappedAction = "read"
        logEntry.document_id = "bbs_thread" + "|" + ids.head
      case (8 | 9, "event_share") =>
        logEntry.mappedAction = "share"
        logEntry.document_id = "bbs_thread" + "|" + ids.head
      // ============ cms ac = 6,7
      case (6 | 7, "show_news") =>
        logEntry.mappedAction = "read"
        logEntry.document_id = "cms_article" + "|" + ids.head
      case (6 | 7, "list_search") =>
        val keyword = param
        logEntry.mappedAction = "search"
        logEntry.document_id = "cms_search" + "|" + md
            .digest(keyword.getBytes("UTF-8"))
            .map("%02x".format(_))
            .mkString
        logEntry.content = keyword
      case (6 | 7, "event_share") =>
        logEntry.mappedAction = "share"
        logEntry.document_id = "cms_article" + "|" + ids.head
      // ============ ac = 4,5
      case (4 | 5, "show_news") =>
        logEntry.mappedAction = "read"
        logEntry.document_id = "cms_article" + "|" + ids.head
      case (4 | 5, act) if act.startsWith("list_search") =>
        val keyword = param
        logEntry.mappedAction = "search"
        logEntry.document_id = "drug_search" + "|" + md
            .digest(keyword.getBytes("UTF-8"))
            .map("%02x".format(_))
            .mkString
        logEntry.content = keyword
      case (4 | 5, act) if act.startsWith("show_drug") =>
        logEntry.mappedAction = "read"
        logEntry.document_id = "drug" + "|" + ids.head
      case (4 | 5, "show_disease") =>
        logEntry.mappedAction = "read"
        logEntry.document_id = "drug_disease" + "|" + ids.head
      case (4 | 5, "event_share") if category == "drug" =>
        logEntry.mappedAction = "share"
        logEntry.document_id = "drug" + "|" + ids.head
      case (4 | 5, "event_share") if category == "news" && ids.nonEmpty =>
        logEntry.mappedAction = "share"
        logEntry.document_id = "cms_article" + "|" + ids.head
      case other_cases =>
    }

    logEntry // return a log entry
  }

  /**
    * BBS Post mapper
    */
  def bbsPostDocumentMap: Row => UserActionLogEntry = {
    case Row(dxyid: String, id: Long, action: String, atime: Long) =>
      if (!dxyid.isEmpty && !action.isEmpty)
        UserActionLogEntry(HBaseUtil.stringToMD5(dxyid),
                           U_TYPE,
                           atime = atime,
                           action = action,
                           mappedAction = action match {
                             case "event_post-new-success" => "new"
                             case "event_post-reply-success" => "reply"
                           },
                           document_id = "bbs_thread" + "|" + id,
                           content = "",
                           account = "10002-4")
      else UserActionLogEntry("", "", 0L, "", "", "", "", "")
  }
}
