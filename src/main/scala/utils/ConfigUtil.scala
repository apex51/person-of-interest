package utils

/**
  * Created by jianghao on 1/9/17.
  */

object ConfigUtil {

  /**
    * === Constants and Configurable Variables ===
    */
  // kafka topics
  val webPVTopic = "PI-web_pv_user_log_test"
  val appPVTopic = "PI-app_pv_user_log_test"
  val appEVTopic = "PI-app_ev_user_log_test"
  val bbsPostTopic = "PI-bbs_post"
  val contentExtractTopic = "PI-content_extract"
  val userInfoTopic = "PI-user_info"
  // kafka parameters
  val kafkaBrokerList =
      "kafka1.lxc.host.dxy:9092,kafka2.lxc.host.dxy:9092,kafka3.lxc.host.dxy:9092"
  val kafkaParams = Map("metadata.broker.list" -> kafkaBrokerList)
  val zkQuorum = "kylin1.uc.host.dxy,master1.uc.host.dxy,master2.uc.host.dxy"
  val zkClientPort = "2181"

  /**
    * === user_info configurations ===
    */
  val userInfoStreamingIntervalSeconds = 1
  val userInfoAppStreamOffsetPath = "pi/checkpoint/user_info/app_stream"
  val userInfoWebStreamOffsetPath = "pi/checkpoint/user_info/web_stream"
  val userInfoBBSPostStreamOffsetPath = "pi/checkpoint/user_info/bbs_post_stream"
  val userInfoHTable = "pi_user_info"


  /**
    * === user_action configurations ===
    */
  val userActionStreamingIntervalSeconds = 1
  val userActionWebPVOffsetPath = "pi/checkpoint/user_action/webPV"
  val userActionAppPVOffsetPath = "pi/checkpoint/user_action/appPV"
  val userActionBBSPostOffsetPath = "pi/checkpoint/user_action/bbsPost"
  val userActionHTable = "pi_user_log"
  val documentContentHTable = "pi_document_content"
  val documentUserActionHTable = "pi_document_user_action"



}
