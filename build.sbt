/**
  * Created by jiangh on 2016-11-21.
  */

import sbt.Keys._


// Config this project.
lazy val root = (project in file(".")) // Add sub-project dependency.
  .settings(

  name := "user-profiling-sys",
  version := "1.0",
  scalaVersion := "2.10.5",

  resolvers += "Cloudera" at "https://repository.cloudera.com/artifactory/cloudera-repos/",

  libraryDependencies += "org.apache.spark" %% "spark-core" % "1.6.0" % "provided",
  libraryDependencies += "org.apache.spark" %% "spark-streaming" % "1.6.0" % "provided",
  libraryDependencies += "org.apache.spark" %% "spark-hive" % "1.6.0" % "provided",
  libraryDependencies += "org.apache.spark" %% "spark-streaming-kafka" % "1.6.0" % "provided",
  libraryDependencies += "org.apache.hbase" % "hbase-common" % "1.2.0-cdh5.7.1" % "provided",
  libraryDependencies += "org.apache.hbase" % "hbase-client" % "1.2.0-cdh5.7.1" % "provided",
  libraryDependencies += "org.apache.hbase" % "hbase-server" % "1.2.0-cdh5.7.1" % "provided",
  libraryDependencies += "org.json4s" %% "json4s-jackson" % "3.2.10" % "provided", // json support
  libraryDependencies += "org.elasticsearch.client" % "transport" % "5.0.0", // java
  libraryDependencies += "org.apache.logging.log4j" % "log4j-api" % "2.7", // java
  libraryDependencies += "org.apache.logging.log4j" % "log4j-core" % "2.7", // java
  libraryDependencies += "org.apache.kafka" % "kafka-clients" % "0.9.0-kafka-2.0.0", // java
  libraryDependencies += "log4j" % "log4j" % "1.2.17",
  libraryDependencies += "org.yaml" % "snakeyaml" % "1.17",

  assemblyMergeStrategy in assembly := {
    case PathList("org", "apache", "logging", "log4j", "core", "impl", "ThrowableProxy$CacheEntry.class") =>
      MergeStrategy.last
    case PathList("org", "apache", "logging", "log4j", "core", "impl", "ThrowableProxy.class") => MergeStrategy.last
    case PathList("org", "apache", "logging", "log4j", "core", "jmx", "Server.class") => MergeStrategy.last
    case PathList("META-INF", "io.netty.versions.properties") => MergeStrategy.last
    case x =>
      val oldStrategy = (assemblyMergeStrategy in assembly).value
      oldStrategy(x)
  },

  assemblyShadeRules in assembly := Seq(
    ShadeRule.rename("com.fasterxml.jackson.**" -> "elastic.com.fasterxml.jackson.@1").inAll,
    ShadeRule.rename("io.netty.**" -> "elastic.io.netty.@1").inAll
  )

)

